# _How To Think Like A Mathematician_ by Kevin Houston

## Chapter 1

#### Rules of sets

- Sets do not have order.

- Members of nested sets are not necessarily members of their outer set.

    - For example, let A = {1, 2, {3, 4}} and B = {3, 4}.

        - Neither 3 nor 4 is a member of A.

        - However, B is a member of A.

- A proper subset contains some of another set, but they are not the same set.

    - For example, let A = {1, 2, {3, 4}} and B = {3, 4}.

        - B ⊂ A and B ≠ A.

        - Which is similar to B < A, rather than B ≤ A.

    - For example, N ⊂ Z ⊂ Q ⊂ R.

        - Positive integers belong to integers.

        - Integers beong to rational numbers.

        - Rational numbers belong to real numbers.

- The cardinality of a finite set is the count of its members.

    - For example, TODO

    - TODO

- The difference between sets is self-explanatory.

    - R / Z is the set of real numbers excluding integers.

        - This can be expressed as ...(-1, 0), (0, 1), (1, 2)....
            
        - In contrast to ...[-1, 0], [0, 1], [1, 2]...

    - For example, <COMPLEMENT>

        - This is a special case: the complement of TODO

    - For example, <NON COMPLEMENT>
    
#### Primitive sets

- Z is the set of all integers.

    - The symbol is derived from the German word for numbers _zahlen_.

    <br>

- N is the set of all positive integers.

- Z^+ is the set of all non-negative integers.

    - Therefore, Z^+ is the union of {0} and N.

    <br>
    
- Q is the set of all rational numbers.

- ∅ is the empty set.
