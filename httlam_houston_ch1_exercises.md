# _How To Think Like A Mathematician_ by Kevin Houston

## Chapter 1 Exercises

#### Exercise 1.10

![exercises_1.10](exercises_1.10.jpg)

#### Questions

- _Why is the empty set not included in the cardinality of sets in which it's not explicitly specified?_ My question answers itself. It's helpful to think about a set as an array from a programming language: not every array contains an empty array as an element. Just because the empty set is a subset of every set does not mean that it is a _member_ of every set.

#### Excersie 1.20 to 1.24

![exercises_1.20_to_1.24](images/exercises_1.20_to_1.24.jpg)

#### Comments

- There are multiple ways to write the solution to the union problem in Exercise 1.24. I crossed out _and 7_ so that I could apply the ≥ symbol (because it's _so_ much tidier to have a crossed out line).

- _I misread this [1.23.2] as the intersection of all three_. I meant (Z ∩ Z) ∩ (Z ∩ ∅) ∩ (Z ∩ R). Let's check. What would that be?

    - Z ∩ Z = Z.
    - Z ∩ ∅ = ∅.
    - Z ∩ R = Z.
    - Z ∩ ∅ ∩ Z = ∅.